<?php 
	require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
  require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Article/Article.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Comment/Comment.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Like/Like.php');

  
	$session = new Session();
  $modelArticle = new Article();
  $modelComment = new Comment();
  $modelLikeArticle = new Like('article');
  $modelLikeComment = new Like('comment');

  
  
  $article = $modelArticle->retrieveById($_GET['id']);
  $user = $session->getAttribute('user');
  $likes = $modelLikeArticle->retrieveById($article->id_article);
  $hasLiked = $modelLikeArticle->retrieveByUser($user, $article->id_article);
  $classLike = '';
  $dataFormLike = '';
  $dataFormDislike = '';
  if ($hasLiked !== false) {
    $classLike = 'active';
    if($hasLiked == 1){
      $dataFormLike = 'delete';
      $dataFormDislike = 'update';
    }else{
      $dataFormLike = 'update';
      $dataFormDislike = 'delete';
    }
  }else{
    $dataFormDislike = $dataFormLike = 'create';
  }

  $comments = $modelComment->retrieveByArticle($article);
  $dateArticle = new DateTime($article->create_date);

  $sessionOpen = false;
  if (isset($_SESSION) && !empty($_SESSION)) {
    $sessionOpen = true;
    if(isset($_POST) && !empty($_POST)){
      $comment = new stdClass();
      $comment->title_comment = $_POST['title'];
      $comment->content_comment = $_POST['content'];
      $comment->user_id= $user->id;
      $comment->article_id = $article->id_article;
      if($modelComment->create($comment)){
        $session->setFlash('msg', ['success' => 'Votre commentaire < '.$comment->title_comment.' > à été posté !']);
        header('Location: /article.php?id='.$_GET['id']);
        exit();
      }else{
        $session->setFlash('msg', ['error' => 'Une erreur est survenue lors du poste de votre commentaire !']);
        header('Location: /article.php?id='.$_GET['id']);
        exit();
      }
    }
  }

?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/head.php') ?>

  <section class="banner">
    <div class="banner-news">
      <img src="/resource/article/<?= $article->img ?>" alt="">
      <div class="banner-info">
        <div class="opacity">
          <h1 class="neon"><?= $article->title ?></h1>
        </div>
      </div>
    </div>
  </section>
  <section class="news-content">
    <div class="panel">
      <div class="news-content">
        <?= nl2br($article->content) ?></div>
      <div class="news-footer">
      <?php
        foreach ($article->tags as $t) {
          ?>
            <div class="tags"><?= $t->name_tag ?></div>
          <?php
        }
      ?>
      <p class="quote">Créé par <?= $article->pseudo ?> le <?= date_format($dateArticle, 'd.m.Y') ?> à <?= date_format($dateArticle, 'H:i') ?></p>
      <p class="like-right"><span class="btn-like like <?php if($hasLiked == 1){ echo $classLike;}?>" data-user="<?= $article->user_id ?>" data-form="<?= $dataFormLike ?>" data-value="1" data-id="<?= $article->id_article ?>" data-table="article"><i class="fas fa-thumbs-up"></i> <?= $likes->likes ?></span> <span class="btn-like dislike <?php if($hasLiked == 0){ echo $classLike;}?>" data-user="<?= $article->user_id ?>" data-form="<?= $dataFormDislike ?>" data-value="0" data-id="<?= $article->id_article ?>" data-table="article"><i class="fas fa-thumbs-down"></i> <?= $likes->dislikes ?></span></p>
      <div class="like-bar">
        <?php
          if ($likes->likes+$likes->dislikes == 0) {
            $widthValue = '50%';
          }else{
            $calc = (100/($likes->likes+$likes->dislikes))*$likes->likes;
            $widthValue = $calc.'%';
          }
        ?>
        <div class="progress-like" style="width:<?= $widthValue ?>"></div>
      </div>
      </div>
    </div>
  </section>
  <section class="form-comment">

    <h2 class="page-title">Commentaires</h2>

    <?php if($sessionOpen): ?>
      <form action="" method="post" class="form">
      <div class="form-group">
        <input type="text" name="title" placeholder="Titre" class="i-100 input alice">
      </div>
      <div class="form-group">
        <textarea name="content" class="i-100 textarea" placeholder="Contenu de votre commentaire"></textarea>
      </div>
      <div class="form-group">
        <button type="submit" class="btn">Poster</button>
      </div>
      </form>
    <?php else: ?>
      <div class="msg info-msg">
        <p class="msg-icon"><i class="fas fa-info"></i></p>
        <p class="msg-content">Vous ne pouvez posté un commentaire lorsque vous été connécté !</p>
      </div>
    <?php endif; ?>
  </section>
  <section class="news-comments">
    <?php
      foreach ($comments as $c) {
        $dateComment = new DateTime($c->create_date_comment);
        $likes = $modelLikeComment->retrieveById($c->id_comment);
        $hasLiked = $modelLikeComment->retrieveByUser($user, $c->id_comment);
        $classLike = '';
        $dataFormLike = '';
        $dataFormDislike = '';
        if ($hasLiked !== false) {
          $classLike = 'active';
          if($hasLiked == 1){
            $dataFormLike = 'delete';
            $dataFormDislike = 'update';
          }else{
            $dataFormLike = 'update';
            $dataFormDislike = 'delete';
          }
        }else{
          $dataFormDislike = $dataFormLike = 'create';
        }
        if ($likes->likes+$likes->dislikes == 0) {
          $widthValue = '50%';
        }else{
          $calc = (100/($likes->likes+$likes->dislikes))*$likes->likes;
          $widthValue = $calc.'%';
        }
        ?>
          <div class="comment">
            <?php if($session->getAttribute('auth')): ?>
              <?php if($user->id == $c->user_id || $user->id_grade == 2): ?>
                <a href="/resource/function/deleteComment.php?id=<?= $c->id_comment ?>" class="comment-delete"><i class="fas fa-trash"></i></a>
              <?php endif; ?>
            <?php endif;?>
            <h3><?= $c->title_comment ?></h3>
            <div class="comment-content">
              <?= $c->content_comment ?>
            </div>
            <div class="comment-footer">
              <p class="quote">Postée par <?= $c->pseudo ?> le <?= date_format($dateComment, 'd.m.Y') ?> à <?= date_format($dateComment, 'H:i') ?></p>
              <p class="like-right"><span class="btn-like like <?php if($hasLiked == 1){ echo $classLike;}?>" data-user="<?= $c->user_id ?>" data-form="<?= $dataFormLike ?>" data-value="1" data-id="<?= $c->id_comment ?>" data-table="comment"><i class="fas fa-thumbs-up"></i> <?= $likes->likes ?></span> <span class="btn-like dislike <?php if($hasLiked == 0){ echo $classLike;}?>" data-user="<?= $c->user_id ?>" data-form="<?= $dataFormDislike ?>" data-value="0" data-id="<?= $c->id_comment ?>" data-table="comment"><i class="fas fa-thumbs-down"></i> <?= $likes->dislikes ?></span></p>
              <div class="like-bar">
                <div class="progress-like" style="width:<?= $widthValue ?>"></div>
              </div>
            </div>
          </div>
        <?php  
      }
    ?>
  </section>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/footer.php') ?>