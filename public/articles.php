<?php 
	require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
	require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Article/Article.php');
	$session = new Session();
	$modelArticle = new Article();

	$articles = $modelArticle->retrieveAll();
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/head.php'); ?>

    <h1 class="page-title">Actualités</h1>

		<section class="last-news">
			<div class="news-container">
				<?php

					foreach ($articles as $a) {
						?>
						<article class="news">
							<div class="news-banner">
								<img src="/resource/article/<?= $a->img ?>">
								<div class="opacity">
									<h3><?= $a->title ?></h3>
								</div>
							</div>
							<div class="news-content">
								<?= substr($a->content,0,300) ?> ...
							</div>
							<div class="news-footer">
								<div class="news-tags">
									<?php
										foreach ($a->tags as $t) {
											?>
												<span class="tags"><?= $t->name_tag ?></span>
											<?php
										}
									?>
								</div>
								<div class="news-link">
									<a href="/article.php?id=<?= $a->id_article ?>">Voir la news</a>
								</div>
							</div>
						</article>
						<?php
					}
				?>
			</div>
		</section>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/footer.php') ?>