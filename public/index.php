<?php 
	require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
	require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Article/Article.php');
	$session = new Session();
	$modelArticle = new Article();

	$articles = $modelArticle->retrieveByAmount(3);
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/head.php') ?>

		<section class="banner">
			<div class="banner-pattern">
				<div class="banner-info">
					<div class="opacity">
						<h1 class="neon">NEEODEV</h1>
						<h2 class="neon">Twitch streamer Sciences & Technologies</h2>
					</div>
				</div>
			</div>
		</section>
		<section class="last-news">
			<div class="news-container">
				<?php

					foreach ($articles as $a) {
						?>
						<article class="news">
							<div class="news-banner">
								<img src="/resource/article/<?= $a->img ?>">
								<div class="opacity">
									<h3><?= $a->title ?></h3>
								</div>
							</div>
							<div class="news-content">
								<?= substr($a->content,0,300) ?> ...
							</div>
							<div class="news-footer">
								<div class="news-tags">
									<?php
										foreach ($a->tags as $t) {
											?>
												<span class="tags"><?= $t->name_tag ?></span>
											<?php
										}
									?>
								</div>
								<div class="news-link">
									<a href="/article.php?id=<?= $a->id_article ?>">Voir la news</a>
								</div>
							</div>
						</article>
						<?php
					}
				?>
			</div>
			<a href="#!" class="btn btn-primary">Voir plus de news</a>
		</section>
		<section class="live">
			<!-- TODO -->
		</section>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/footer.php') ?>