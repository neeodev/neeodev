<?php

  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Session/Session.php');

  $session = new Session();

  if($session->getAttribute('auth')){
    $user_date = date_create($session->getAttribute('user')->create_date_user);
    $now = date_create(date('Y-m-d'));
    $diff = date_diff($user_date,$now);
    echo $diff->format('%a');
  }
