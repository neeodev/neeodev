<?php

  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Session/Session.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Comment/Comment.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/User/User.php');

  $session = new Session();
  $modelComment = new Comment();
  $modelUser = new User();

  if(isset($_GET) && !empty($_GET)){
    $comment = $modelComment->retrieveById($_GET['id']);
    $user = $session->getAttribute('user');
    if($comment->user_id == $user->id || $user->id_grade == 2){
      if($modelComment->delete($comment)){
        if($comment->user_id != $user->id && $user->id_grade == 2){
          $modelUser->decrementUserReputation($comment->user_id, 3);
        }
        $session->setFlash('msg', ['success' => 'Votre commentaire < '.$comment->title_comment.' > à été supprimé !']);
        header('Location: /article.php?id='.$comment->article_id);
        exit();
      }else{
        $session->setFlash('msg', ['error' => 'Un probléme est survenu lors de la suppression de votre commentaire !']);
        header('Location: /article.php?id='.$comment->article_id);
        exit();
      }
    }else{
      $session->setFlash('msg', ['error' => 'C\'est pas bien de tricher, c\'est pas ton commentaire !']);
      header('Location: /article.php?id='.$comment->article_id);
      exit();
    }
  }else{
    $session->setFlash('msg', ['error' => 'Aucun commentaire trouvé !']);
    header('Location: /article.php?id='.$comment->article_id);
    exit();
  }