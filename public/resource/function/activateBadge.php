<?php

  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Session/Session.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Badge/Badge.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/User/User.php');

  $session = new Session();
  $modelBadge = new Badge();
  $modelUser = new User();

  if(isset($_POST) && !empty($_POST)){
    if ($_POST['activate'] == 1) {
      $user = $session->getAttribute('user');
      $badge = $modelBadge->retrieveByID($_POST['badge']);
      if($modelUser->activateBadge($user,$badge)){
        echo 'true';
      }else{
        echo 'has';
      }
    }else{
      echo 'false';
    }
  }else{
    echo 'false';
  }