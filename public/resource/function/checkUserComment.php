<?php

  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Comment/Comment.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Session/Session.php');

  $session = new Session();
  $modelComment = new Comment();

  $user = $session->getAttribute('user');

  $comments = $modelComment->retrieveByUser($user);

  echo count($comments);