<?php

  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Session/Session.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Like/Like.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/User/User.php');

  
  if (isset($_POST) && !empty($_POST)) {

    $session = new Session();
    $user = $session->getAttribute('user');

    $form = $_POST['form'];
    $value = $_POST['value'];
    $id = $_POST['id'];
    $table = $_POST['table'];
    $user_id = $_POST['user'];

    $like = new stdClass();
    $like->{$table.'_id'} = $id;
    $like->user_id = $user->id;
    $like->{'like_'.$table} = $value;

    $modelLike = new Like($table);
    $modelUser = new User();

    $post_user = $modelUser->retrieveById($user_id);
    
    $modelLike->{$form}($like);

    switch ($form) {
      case 'create':
          if($value == 1){
            $modelUser->incrementUserReputation($post_user, 1);
            echo 'increment 1';
          }else{
            $modelUser->decrementUserReputation($post_user, 1);
            echo 'Decrement 1';
          }
        break;
      case 'update':
          if($value == 1){
            $modelUser->incrementUserReputation($post_user, 2);
            echo 'increment 2';
          }else{
            $modelUser->decrementUserReputation($post_user, 2);
            echo 'Decrement 2';
          }
        break;
      case 'delete':
          if($value == 1){
            $modelUser->decrementUserReputation($post_user, 1);
            echo 'Decrement 1';
          }else{
            $modelUser->incrementUserReputation($post_user, 1);
            echo 'Increment 1';
          }
        break;
      
      default:
        break;
    }
  }