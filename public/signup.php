<?php
	require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
	
	if (isset($_POST) && !empty($_POST)) {
		require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Security/Security.php');
		
		$security = new Security();
		$register = $security->register((object)$_POST);

		if ($register) {
			header('Location: /');
			exit();
		}else{
			$session = new Session();
		}
	}else{
		$session = new Session();
	}
?>


<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/head.php') ?>

	<h1 class="page-title">Inscription</h1>

	<form action="" method="post" class="form">
		<?php 
			if($session->hasFlash()){ 
				$flash = (object) $session->getFlash()->user_info; 
			}else{
				$flash = null;
			} 
		?>
		<div class="form-group">
			<input type="text" name="forname" placeholder="Prenom" class="i-50 input" value="<?php if($flash != null) echo $flash->forname ?>">
			<input type="text" name="name" placeholder="Nom" class="i-50 input" value="<?php if($flash != null) echo $flash->name ?>">
		</div>
		<div class="form-group">
			<input type="text" name="pseudo" placeholder="Pseudo" class="i-30 input" value="<?php if($flash != null) echo $flash->pseudo ?>">
			<input type="text" name="mail" placeholder="E-Mail" class="i-70 input" value="<?php if($flash != null) echo $flash->mail ?>">
		</div>
		<div class="form-group">
			<input type="password" name="pass" placeholder="Mot de passe..." class="i-50 input">
			<input type="password" name="conf" placeholder="Confirmation mot de passe..." class="i-50 input">
		</div>
		<button type="submit" class="btn">S'inscrire</button>
	</form>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/footer.php') ?>