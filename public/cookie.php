<?php
if (isset($_GET) && !empty($_GET)) {
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Cookie/Cookie.php');
    
    $cookie = new Cookie();
    
    $cookieValue = $_GET['cookie'];
    
    if ($cookieValue == 1) {
        $cookie->setCookie('menu-mini', $cookieValue);
    }elseif($cookieValue == 0){
        $cookie->delete('menu-mini');
    }else{
        header('Location: /');
        exit();
    }
}else{
    header('Location: /');
    exit();
}

