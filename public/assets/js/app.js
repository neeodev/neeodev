$(document).ready(function(){
	$('.menu-small').click(function(){
		$('nav').toggleClass('mini')
		$('.container').toggleClass('big')
		$('.menu-small i.fas').toggleClass('fa-arrow-left')
		$('.menu-small i.fas').toggleClass('fa-arrow-right')

		if ($('.menu-small i.fas').hasClass('fa-arrow-left')) {
			$.ajax({
				url: "/cookie.php?cookie=0"
			})
		}else{
			$.ajax({
				url: "/cookie.php?cookie=1"
			})
		}
	})
	$('.menu-burger').click(function(){
		$('nav').toggleClass('open')
		$('.menu-burger i.fas').toggleClass('fa-bars')
		$('.menu-burger i.fas').toggleClass('fa-times')
	})


	//Badge durée sur le site
	$.ajax({
		url: "/resource/function/checkUserTime.php"
	}).done(function(data){
		let badge
		let badgeName = ''
		if(data > 182){
			badge = 3
			badgeName = 'Débutant !'
		}
		if(data > 365){
			badge = 4
			badgeName = 'Semi-pro !'
		}
		if(data > 730){
			badge = 5
			badgeName = 'Pro !'
		}
		if(data > 1825){
			badge = 6
			badgeName = 'Vétéran !'
		}

		$.ajax({
			url: "/resource/function/activateBadge.php",
			type : 'POST',
			data : 'activate=1&badge=' + badge
		}).done(function(data){
			if(data == 'true'){
				alert('Badge < '+ badgeName +' > Dévérouillé.')
			}
		})
	})

	//badge commentaires - Star wars
	$.ajax({
		url: "/resource/function/checkUserComment.php"
	}).done(function(data){
		let badge
		let badgeName = ''

		switch (data) {
			case '3':
				badge = 7
				badgeName = 'Jeune padawan !'
				break;
		
			case '10':
				badge = 8
				badgeName = 'Jedi tu es !'
				break;

			default:
				if(data > 3 && data < 10){
					badge = 7
					badgeName = 'Jeune padawan !'
				}else if(data > 10){
					badge = 8
					badgeName = 'Jedi tu es !'
				}
				break;
		}

		$.ajax({
			url: "/resource/function/activateBadge.php",
			type : 'POST',
			data : 'activate=1&badge=' + badge
		}).done(function(data){
			if(data == 'true'){
				alert('Badge < '+ badgeName +' > Dévérouillé.')
			}
		})
	})

	//Badge commentaires - Effacée de la réalité
	$('.alice').keyup(function(){
		if($('.alice').val() == 'Effacez-moi'){
			$.ajax({
				url: "/resource/function/activateBadge.php",
				type : 'POST',
				data : 'activate=1&badge=9'
			}).done(function(data){
				if(data == 'true'){
					alert('Badge < Effacée de la réalité ! > Dévérouillé.')
					window.location = '/account.php'
				}else if(data == 'has'){
					alert('Vous possédé déjà le badge < Effacée de la réalité ! >.')
					window.location = '/account.php'
				}else{
					window.location = '/'
				}
			})
		}
	})

	$('.comment-delete').click(function(){
		$.ajax({
			url: "/resource/function/activateBadge.php",
			type : 'POST',
			data : 'activate=1&badge=10'
		}).done(function(data){
			if(data == 'true'){
				alert('Badge < Petrificus totalus ! > Dévérouillé.')
			}
		})
	})

	$('.btn-like').click(function(){
		let form = $(this).data('form')
		let value = $(this).data('value')
		let id = $(this).data('id')
		let table = $(this).data('table')
		let user = $(this).data('user')

		$.ajax({
			url: "/resource/function/like.php",
			type: 'POST',
			data: 'form='+form+'&value='+value+'&id='+id+'&table='+table+'&user='+user
		}).done(function(){
			location.reload(true)
		})
	})

	//badge pirate des caraïbes - bouton j'aime
		//J'ai cru que... Que je l'aimais ? - Mais si tu ne me dis rien comment pourrais-je te faire confiance.

	//badge fight club - bouton j'aime pas
		//T'es la pire chose qui me soit jamais arrivée !

	//badge jeux d'enfant - bouton j'aime
		//Dis moi que tu m'aimes, rien qu'une fois

	//badge La fin est proche ! - Suppression de compte
		//La vie et la mort c'est un bel exemple d'amour, La vie donne des cadeaux à la mort mais il est égoïste

	//badge L'insulte est l'arme du faible ! - Suppresion commentaire par un modo
		//Le crachat que tu jettes dans le ciel te retombe sur la face

	$.ajax({
		url: "/resource/function/checkUserReputation.php"
	}).done(function(data){
		if(data < 50){
			$.ajax({
				url: "/resource/function/activateBadge.php",
				type : 'POST',
				data : 'activate=1&badge=11'
			}).done(function(data){
				if(data == 'true'){
					alert('Badge < L\'insulte est l\'arme du faible ! > Dévérouillé.')
				}
			})
		}
	})
	
	let keyOrder = ['38','38','40','40','37','39','37','39','66','65'];
	let keyUser = [];

	$(document).keydown(function(event){
		var keycode = (event.keyCode ? event.keyCode : event.which)
		let i = keyUser.length
		if(keycode == keyOrder[i]){
			keyUser.push(keycode);
			if(i == 9){
				$.ajax({
					url: "/resource/function/activateBadge.php",
					type : 'POST',
					data : 'activate=1&badge=1'
				}).done(function(data){
					if(data == 'true'){
						alert('Badge < Retro gamer dans l\'âme ! > Dévérouillé.')
						window.location = '/account.php'
					}else if(data == 'has'){
						alert('Vous possédé déjà le badge < Retro gamer dans l\'âme ! >.')
						window.location = '/account.php'
					}else{
						window.location = '/'
					}
				})
				keyUser = []
			}
		}else{
			keyUser = []
		}

		if(keycode == '123'){
			$.ajax({
				url: "/resource/function/activateBadge.php",
				type : 'POST',
				data : 'activate=1&badge=2'
			}).done(function(data){
				if(data == 'true'){
					alert('Badge < Programmer expérimenté ! > Dévérouillé.')
					window.location = '/account.php'
				}else if(data == 'has'){
					alert('Vous possédé déjà le badge < Programmer expérimenté ! >.')
					window.location = '/account.php'
				}else{
					window.location = '/'
				}
			})
		}
	})
})