<?php

    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');

    $session = new Session();
    $admin = new Admin();

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/head.php'); ?>

    <h1 class="page-title"">Administration</h1>
    <h2 class="page-subtitle">Bienvenu monsieur l'admin <?= $session->getAttribute('user')->pseudo ?></h2>
    <h2 class="page-subtitle">Un grand pouvoir implique de grandes résponsabilités !</h2>
    <h2 class="page-subtitle"><span class="time-left">10</span> secondes</h2>
    <script>
        setInterval(function(){
            $('.time-left').text($('.time-left').text() - 1);
        }, 1000)
        setTimeout(function(){
            window.location = "/admin/user.php";
        }, 10000);
    </script>
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/footer.php') ?>