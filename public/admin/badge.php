<?php
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Badge/Badge.php');

    $session = new Session();
    $admin = new Admin();
    $modelBadge = new Badge();

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/head.php') ?>

    <h1 class="page-title">Listes des badges</h1>

    <table class="list">
        <thead>
            <tr>
                <th>Nom</th>
                <th>Description</th>
                <th>Icon</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $badges = $modelBadge->retrieveAll();

                foreach ($badges as $b) {
                    ?>
                        <tr>
                            <td><?= $b->name_badge ?></td>
                            <td><?= $b->description_badge ?></td>
                            <td><?= $b->icon_badge ?></td>
                            <td>
                                <a href="/admin/badge/edit.php?id=<?= $b->id_badge ?>" class="btn-action btn-edit"><i class="fas fa-pen"></i></a>
                                <a href="/admin/badge/delete.php?id=<?= $b->id_badge ?>" class="btn-action btn-delete"><i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    <?php
                }
            ?>
        </tbody>
    </table>
    <a href="/admin/badge/create.php" class="btn">Créer un nouveau badge</a>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/footer.php') ?>