<?php
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Article/Article.php');

    $session = new Session();
    $admin = new Admin();
    $modelArticle = new Article();

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }

    $article = $modelArticle->retrieveById($_GET['id']);

    if (isset($_POST) && !empty($_POST)) {
        if ('confirmation' === $_POST['conf']) {
            if($modelArticle->delete($article)){
                $session->setFlash('msg', ['success' => 'L\'article < '. $article->title .' > à bien été supprimé !']);
                header('Location: /admin/article.php');
                exit();
            }else{
                $session->setFlash('msg', ['error' => 'Un probléme est survenu lors de la suppression !']);
                header('Location: /admin/article.php');
                exit();
            }
        }else{
            $session->setFlash('msg', ['error' => 'Le mot de confirmation à été tapé faux !']);
            header('Location: /admin/article.php');
            exit();
        }
    }
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/head.php'); ?>

    <h1 class="page-title">Suppression de l'article "<?= $article->title ?>"</h1>
    <form action="" method="post" class="form">
        <div class="form-group">
            <input type="text" name="conf" placeholder="Veillez taper le mot confirmation..." class="input i-100">
        </div>
        <div class="form-group">
        <button type="submit" class="btn btn-validate">Supprimer</button>
        <a href="/admin/article.php" class="btn btn-cancel">Annuler</a>
        </div>
    </form>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/footer.php') ?>