<?php
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Article/Article.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Image/Image.php');

    $session = new Session();
    $admin = new Admin();
    $modelArticle = new Article();
    $modelImage = new Image('article');

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }

    if (isset($_POST) && !empty($_POST) && isset($_FILES) && !empty($_FILES)) {

        $article = new stdClass();
       
        $tags = explode('; ',$_POST['tags']);
        $article->title = $_POST['title'];
        $article->content = $_POST['content'];
        $article->img = (object) $_FILES['img'];
        $article->user_id = $session->getAttribute('user')->id;
        $article->img = $modelImage->upload($article->img);
        $article->tags = new stdClass();

        foreach ($tags as $t) {
            $article->tags->$t = new stdClass();
            $article->tags->$t->name_tag = $t;
        }

        if($modelArticle->create($article)){
            $session->setFlash('msg', ['success' => 'La création de l\'article < '.$article->title.' > à été terminé correctement !']);
            header('Location: /admin/article.php');
            exit();
        }else{
            $session->setFlash('msg', ['error' => 'Une erreur est survenu lors de la création de l\'article !']);
            header('Location: /admin/article.php');
            exit();
        }
    }
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/head.php'); ?>

    <h1 class="page-title">Création d'un article</h1>
    <form action="" method="post" class="form" enctype='multipart/form-data'>
        <div class="form-group">
			<input type="text" name="title" placeholder="Titre" class="i-100 input" value="">
		</div>
        <div class="form-group">
			<textarea name="content" class="i-100 textarea" placeholder="Contenu de l'article"></textarea>
        </div>
		<div class="form-group">
			<input type="file" name="img" placeholder="Image" class="i-30 input" value="">
			<input type="text" name="tags" placeholder="Tags séparé par un ; Exemple: pomme; Peche; ..." class="i-70 input" value="">
		</div>
        <div class="form-group">
            <button type="submit" class="btn btn-validate">Creer</button>
            <a href="/admin/article.php" class="btn btn-cancel">Annuler</a>
        </div>
    </form>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/footer.php') ?>