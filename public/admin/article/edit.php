<?php
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Article/Article.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Image/Image.php');


    $session = new Session();
    $admin = new Admin();
    $modelArticle = new Article();
    $modelImage = new Image('article');

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }

    $article = $modelArticle->retrieveById($_GET['id']);
    $tagsString = '';
    foreach ($article->tags as $t) {
        $tagsString = $tagsString . $t->name_tag . '; '; 
    }
    $tagsString = substr($tagsString, 0, -2);

    if (isset($_POST) && !empty($_POST)) {
       
        $tags = explode('; ',$_POST['tags']);
        $article->title = $_POST['title'];
        $article->content = $_POST['content'];
        if (isset($_FILES) && !empty($_FILES) && $_FILES['img']['name'] != '') {
            $article->img = $modelImage->update((object) $_FILES['img'], $article->img);
        }

        $article->tags = new stdClass();

        foreach ($tags as $t) {
            $article->tags->$t = new stdClass();
            $article->tags->$t->name_tag = $t;
        }

        if ($modelArticle->update($article)) {
            $session->setFlash('msg', ['success' => 'L\'Article < '.$article->title,' > à bien été modifié !']);
            header('Location: /admin/article.php');
            exit();
        }else{
            $session->setFlash('msg', ['error' => 'Une erreur est survenu lors de la modification !']);
            header('Location: /admin/article.php');
            exit();
        }
    }
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/head.php'); ?>

    <h1 class="page-title">Edition de l'article "<?= $article->title ?>"</h1>
    <form action="" method="post" class="form" enctype='multipart/form-data'>
        <div class="form-group">
			<input type="text" name="title" placeholder="Titre" class="i-100 input" value="<?= $article->title ?>">
		</div>
        <div class="form-group">
			<textarea name="content" class="i-100 textarea"><?= $article->content ?></textarea>
        </div>
		<div class="form-group">
			<input type="file" name="img" placeholder="Images" class="i-30 input" value="">
			<input type="text" name="tags" placeholder="Tags" class="i-70 input" value="<?= $tagsString ?>">
		</div>
        <div class="form-group">
        <button type="submit" class="btn btn-validate">Editer</button>
        <a href="/admin/article.php" class="btn btn-cancel">Annuler</a>
        </div>
    </form>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/footer.php') ?>