<?php
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Article/Article.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Tag/Tag.php');

    $session = new Session();
    $admin = new Admin();
    $modelArticle = new Article();

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/head.php') ?>

    <h1 class="page-title">Listes des articles du site</h1>

    <table class="list">
        <thead>
            <tr>
                <th>titre</th>
                <th>Auteur</th>
                <th>Date de création</th>
                <th>Date de modification</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $articles = $modelArticle->retrieveAll();

                foreach ($articles as $a) {
                    ?>
                        <tr>
                            <td><?= $a->title ?></td>
                            <td><?= $a->pseudo ?></td>
                            <td><?= $a->create_date ?></td>
                            <td><?= $a->update_date ?></td>
                            <td>
                                <a href="/admin/article/edit.php?id=<?= $a->id_article ?>" class="btn-action btn-edit"><i class="fas fa-pen"></i></a>
                                <a href="/admin/article/delete.php?id=<?= $a->id_article ?>" class="btn-action btn-delete"><i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    <?php
                }
            ?>
        </tbody>
    </table>
    <a href="/admin/article/create.php" class="btn">Créer un nouvel article</a>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/footer.php') ?>