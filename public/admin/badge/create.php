<?php
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Badge/Badge.php');

    $session = new Session();
    $admin = new Admin();
    $modelBadge = new Badge();

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }

    if (isset($_POST) && !empty($_POST)) {

        $badge = new stdClass();
       
        $badge->name_badge = $_POST['name'];
        $badge->description_badge = $_POST['description'];
        $badge->icon_badge = $_POST['icon'];

        if($modelBadge->create($badge)){
            $session->setFlash('msg', ['success' => 'La création de votre badge < '.$badge->name.' > à été terminé correctement !']);
            header('Location: /admin/badge.php');
            exit();
        }else{
            $session->setFlash('msg', ['error' => 'Une erreur est survenu lors de la création de votre badge !']);
            header('Location: /admin/badge.php');
            exit();
        }
    }
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/head.php'); ?>

    <h1 class="page-title">Création d'un badge</h1>
    <form action="" method="post" class="form" enctype='multipart/form-data'>
      <div class="form-group">
        <input type="text" name="name" placeholder="Nom du badge" class="i-50 input" value="">
        <input type="text" name="icon" placeholder="Nom de l'icon font awesome" class="i-50 input" value="">
      </div>
      <div class="form-group">
        <textarea name="description" class="i-100 textarea" placeholder="Description du badge"></textarea>
      </div>
      <div class="form-group">
        <button type="submit" class="btn btn-validate">Creer</button>
        <a href="/admin/badge.php" class="btn btn-cancel">Annuler</a>
      </div>
    </form>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/footer.php') ?>