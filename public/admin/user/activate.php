<?php

    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/User/User.php');

    $session = new Session();
    $admin = new Admin();
    $modelUser = new User();

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }

    $user = $modelUser->retrieveById($_GET['id']);

    if($modelUser->setActiveByID($user->id)){
        $session->setFlash('msg', ['success' => 'Le compte '.$user->pseudo,' à bien été activé !']);
        header('Location: /admin/user.php');
        exit();
    }else{
        $session->setFlash('msg', ['error' => 'Erreur lors de l\'activation du compte'. $user->pseudo]);
        header('Location: /admin/user.php');
        exit();
    }
    