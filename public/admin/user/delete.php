<?php
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/User/User.php');

    $session = new Session();
    $admin = new Admin();
    $modelUser = new User();

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }

    $user = $modelUser->retrieveById($_GET['id']);

    if (isset($_POST) && !empty($_POST)) {
        if ($user->pseudo === $_POST['pseudo']) {
            if($modelUser->delete($user)){
                $session->setFlash('msg', ['success' => 'L\'utilisateur '.$user->pseudo.' a bien été supprimé !']);
                header('Location: /admin/user.php');
                exit();
            }else{
                $session->setFlash('msg', ['error' => 'Un probléme est survenu lors de la suppression !']);
                header('Location: /admin/user.php');
                exit();
            }
        }else{
            $session->setFlash('msg', ['error' => 'Le pseudo que vous avez entré ne correspond pas à celui qui doit être supprimé !']);
            header('Location: /admin/user.php');
            exit();
        }
    }
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/head.php'); ?>

    <h1 class="page-title">Suppression de <?= $user->pseudo ?></h1>
    <form action="" method="post" class="form">
        <div class="form-group">
            <input type="text" name="pseudo" placeholder="Pseudo de confirmation..." class="input i-100">
        </div>
        <div class="form-group">
        <button type="submit" class="btn btn-validate">Supprimer</button>
        <a href="/admin/user.php" class="btn btn-cancel">Annuler</a>
        </div>
    </form>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/footer.php') ?>