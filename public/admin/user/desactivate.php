<?php

    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/User/User.php');

    $session = new Session();
    $admin = new Admin();
    $modelUser = new User();

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }

    $user = $modelUser->retrieveById($_GET['id']);

    if($modelUser->setInactiveByID($user->id)){
        $session->setFlash('msg', ['success' => 'Le compte '.$user->pseudo,' à bien été désactivé !']);
        header('Location: /admin/user.php');
        exit();
    }else{
        $session->setFlash('msg', ['error' => 'Erreur lors de la désactivation du compte'. $user->pseudo]);
        header('Location: /admin/user.php');
        exit();
    }
    