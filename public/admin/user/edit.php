<?php
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/User/User.php');

    $session = new Session();
    $admin = new Admin();
    $modelUser = new User();

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }

    $user = $modelUser->retrieveById($_GET['id']);

    if (isset($_POST) && !empty($_POST)) {
        $user->name = $_POST['name'];
        $user->forname = $_POST['forname'];
        $user->mail = $_POST['mail'];
        $user->pseudo = $_POST['pseudo'];
        if ($modelUser->update($user)) {
            $session->setFlash('msg', ['success' => 'Le compte '.$user->pseudo,' à bien été modifié !']);
            header('Location: /admin/user.php');
            exit();
        }else{
            $session->setFlash('msg', ['error' => 'Une erreur est survenu lors de la modification !']);
            header('Location: /admin/user.php');
            exit();
        }
    }
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/head.php'); ?>

    <h1 class="page-title">Edition de <?= $user->pseudo ?></h1>
    <form action="" method="post" class="form">
    <div class="form-group">
			<input type="text" name="forname" placeholder="Prenom" class="i-50 input" value="<?= $user->forname ?>">
			<input type="text" name="name" placeholder="Nom" class="i-50 input" value="<?= $user->name ?>">
		</div>
		<div class="form-group">
			<input type="text" name="pseudo" placeholder="Pseudo" class="i-30 input" value="<?= $user->pseudo ?>">
			<input type="text" name="mail" placeholder="E-Mail" class="i-70 input" value="<?= $user->mail ?>">
		</div>
        <div class="form-group">
        <button type="submit" class="btn btn-validate">Editer</button>
        <a href="/admin/user.php" class="btn btn-cancel">Annuler</a>
        </div>
    </form>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/footer.php') ?>