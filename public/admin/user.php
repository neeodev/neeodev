<?php
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Admin/Admin.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/User/User.php');

    $session = new Session();
    $admin = new Admin();
    $modelUser = new User();

    if (!$admin->isAdmin()) {
        header('Location: /');
        exit();
    }
?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/head.php'); ?>

    <h1 class="page-title">Listes des utilisateurs</h1>

    <table class="list">
        <thead>
            <tr>
                <th>Pseudo</th>
                <th>Mail</th>
                <th>Active</th>
                <th>Grade</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $users = $modelUser->retrieveAll();

                foreach ($users as $u) {
                    ?>
                        <tr>
                            <td><?= $u->pseudo ?></td>
                            <td><?= $u->mail ?></td>
                            <td>
                                <?php
                                    if($u->active == 0){
                                        echo 'Inactif';
                                    }else{
                                        echo 'Actif';
                                    }
                                ?>
                            </td>
                            <td><?= $u->name_grade ?></td>
                            <td>
                                <?php
                                    if($u->active == 0){
                                        ?><a href="/admin/user/activate.php?id=<?= $u->id ?>" class="btn-action btn-check"><i class="fas fa-check"></i></a><?php
                                    }else{
                                        ?><a href="/admin/user/desactivate.php?id=<?= $u->id ?>" class="btn-action btn-check"><i class="fas fa-times-circle"></i></a><?php
                                    }
                                ?>
                                <a href="/admin/user/edit.php?id=<?= $u->id ?>" class="btn-action btn-edit"><i class="fas fa-pen"></i></a>
                                <a href="/admin/user/delete.php?id=<?= $u->id ?>" class="btn-action btn-delete"><i class="fas fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    <?php
                }
            ?>
        </tbody>
    </table>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/admin/footer.php') ?>