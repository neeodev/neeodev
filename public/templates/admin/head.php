<?php

	require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Cookie/Cookie.php');

	$cookie = new Cookie();
	
	$navClass = '';
	$arrow = 'left';
	$container = '';
	if ($cookie->retrieve('menu-mini') == 1) {
		$navClass = 'class="mini"';
		$arrow = 'right';
		$container = 'big';
	}

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/assets/css/app.css">
	<script src="https://kit.fontawesome.com/086d56b128.js" crossorigin="anonymous"></script>
	<script src="/assets/js/jquery.js"></script>
	<script src="/assets/js/app.js"></script>
	<title>Neeodev</title>
</head>
<body>
	<header>
		<span class="menu-burger"><i class="fas fa-bars"></i></span>
		<nav <?= $navClass ?>>
			<ul>
				<span class="menu-small"><i class="fas fa-arrow-<?= $arrow ?>"></i></span>
				<li><a href="/"><i class="fas fa-home"></i> Accueil</a></li>
				<li><a href="/admin/user.php"><i class="fas fa-users"></i> Gestion Utilisateurs</a></li>
				<li><a href="/admin/badge.php"><i class="fas fa-medal"></i> Gestion Badges</a></li>
				<li><a href="/admin/article.php"><i class="far fa-newspaper"></i> Gestion Articles</a></li>
				<li><a href="#!"><i class="fab fa-twitch"></i> Gestion stream</a></li>
				<li><a href="#!"><i class="fas fa-hand-holding-usd"></i> Gestion Partenaire</a></li>
			</ul>
			<ul>
				<li><a href="/logout.php"><i class="fas fa-sign-out-alt"></i> Déconnexion</a></li>
			</ul>
		</nav>
	</header>
	<div class="container <?= $container ?>">