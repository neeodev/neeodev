<?php

	require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Cookie/Cookie.php');

	$cookie = new Cookie();
	
	$navClass = '';
	$arrow = 'left';
	$container = '';
	if ($cookie->retrieve('menu-mini') == 1) {
		$navClass = 'class="mini"';
		$arrow = 'right';
		$container = 'big';
	}

?>
<!--
	         _nnnn_                      
        dGGGGMMb     ,"""""""""""""".
       @p~qp~~qMb    | Linux Rules! |
       M|@||@) M|   _;..............'
       @,----.JM| -'
      JS^\__/  qKL
     dZP        qKRb
    dZP          qKKb
   fZP            SMMb
   HZM            MMMM
   FqM            MMMM
 __| ".        |\dS"qML
 |    `.       | `' \Zq
_)      \.___.,|     .'
\____   )MMMMMM|   .'
     `-'       `--' hjm

Vous avez débloquer le badge Programmer bravo !!!
-->


<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="/assets/css/app.css">
	<script src="https://kit.fontawesome.com/086d56b128.js" crossorigin="anonymous"></script>
	<script src="/assets/js/jquery.js"></script>
	<script src="/assets/js/app.js"></script>
	<title>Neeodev</title>
</head>
<body>
	<header>
		<span class="menu-burger"><i class="fas fa-bars"></i></span>
		<nav <?= $navClass ?>>
			<ul>
				<span class="menu-small"><i class="fas fa-arrow-<?= $arrow ?>"></i></span>
				<li><a href="/"><i class="fas fa-home"></i> Accueil</a></li>
				<li><a href="#!"><i class="fab fa-twitch"></i> Live</a></li>
				<li><a href="/articles.php"><i class="far fa-newspaper"></i> News</a></li>
				<li><a href="#!"><i class="fas fa-user-friends"></i> Partenaire</a></li>
				<li><a href="#!"><i class="far fa-address-card"></i> A propos</a></li>
			</ul>
			<ul>
				<?php if($session->getAttribute('auth')) : ?>
					<?php if($session->getAttribute('user')->grade_id == 2) : ?>
						<li><a href="/admin"><i class="fas fa-tools"></i>Administration</a></li>
					<?php endif; ?>
					<li><a href="/account.php"><i class="fas fa-user"></i><?= $session->getAttribute('user')->pseudo ?></a></li>
					<li><a href="logout.php"><i class="fas fa-sign-out-alt"></i> Déconnexion</a></li>
				<?php else: ?>
					<li><a href="/signup.php"><i class="fas fa-user-plus"></i> Inscription</a></li>
					<li><a href="/login.php"><i class="fas fa-sign-in-alt"></i> Connexion</a></li>
				<?php endif; ?>
			</ul>
		</nav>
	</header>
	<div class="container <?= $container ?>">