<?php

    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');

    $session = new Session();
    $session->destroy();
    
    header('Location: /');
    exit();