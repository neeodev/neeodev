<?php 
  require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Badge/Badge.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/User/User.php');

  $session = new Session();
  $modelBadge = new Badge();
  $modelUser = new User();
  
  $user = $session->getAttribute('user');
  $badges = $modelBadge->retrieveAll();

?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/head.php') ?>

		<section class="banner">
			<div class="banner-pattern">
				<div class="banner-info">
					<div class="opacity">
            <div class="profil_pic">
              <img src="/resource/user/profil.png" alt="">
            </div>
						<h1 class="neon"><?= $user->pseudo ?></h1>
            <h2 class="neon"><?= $user->name_grade ?></h2>
					</div>
				</div>
			</div>
    </section>
    <section class="badges">
      <h2 class="page-title">Badges</h2>
      <div class="panel badge-container">
        <?php
          foreach ($badges as $b) {
            $hasBadge = $modelUser->checkBadge($user, $b);
            ?>
              <div class="badge">
                <div class="badge-icon <?php if($hasBadge != false){ echo 'active'; } ?>">
                <i class="fas fa-certificate star"></i>
                  <i class="<?= $b->icon_badge ?>"></i>
                </div>
                <div class="badge-name">
                  <p><?= $b->name_badge ?></p>
                </div>
              </div>
            <?php
          }
        ?>
      </div>
    </section>
		
<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/footer.php') ?>