<?php

  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Database/Database.php');
  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Session/Session.php');

  class Like{

    private $session;
    private $pdo;
    private $table;
    private $field;
    private $like;

    public function __construct($table){
      $this->session = new Session();
      $this->pdo = Database::getConnection();
      $this->table = 'likes_'.$table;
      $this->field = $table.'_id';
      $this->like = 'like_'.$table;
    }

    public function retrieveById($id){
      $res = new stdClass();
      $sql = 'SELECT COUNT(*) as likes FROM '.$this->table.' WHERE '.$this->field.' = :id AND '.$this->like.' = 1';
      $req = $this->pdo->prepare($sql);
      $req->execute([':id' => $id]);

      $likes = $req->fetch(PDO::FETCH_OBJ);

      $sql = 'SELECT COUNT(*) as dislikes FROM '.$this->table.' WHERE '.$this->field.' = :id AND '.$this->like.' = 0';
      $req = $this->pdo->prepare($sql);
      $req->execute([':id' => $id]);

      $dislikes = $req->fetch(PDO::FETCH_OBJ);

      $res->likes = $likes->likes;
      $res->dislikes = $dislikes->dislikes;

      return $res;
    }

    public function retrieveByUser($user,$id){
      $res = new stdClass();
      $sql = 'SELECT * FROM '.$this->table.' WHERE '.$this->field.' = :id AND user_id = :user';
      $req = $this->pdo->prepare($sql);
      $req->execute([':id' => $id, ':user' => $user->id]);

      $data = $req->fetch(PDO::FETCH_OBJ);
      if ($data != false) {
        $data = $data->{$this->like};
      }
      return $data;
    }

    //create
    public function create($like){
      $sql = 'INSERT INTO '.$this->table.' VALUES(:field, :user, :value)';
      $req = $this->pdo->prepare($sql);
      $res = $req->execute([':field' => $like->{$this->field}, ':user' => $like->user_id, ':value' => $like->{$this->like}]);

      return $res;
    }

    //update
    public function update($like){
      $sql = 'UPDATE '.$this->table.' SET '.$this->like.' = :value WHERE '.$this->field.' = :field AND user_id = :user';
      $req = $this->pdo->prepare($sql);
      $res = $req->execute([':value' => $like->{$this->like}, ':field' => $like->{$this->field}, ':user' => $like->user_id]);

      return $res;
    }

    //delete
    public function delete($like){
      $sql = 'DELETE FROM '.$this->table.' WHERE '.$this->field.' = :field AND user_id = :user';
      $req = $this->pdo->prepare($sql);
      $res = $req->execute([':field' => $like->{$this->field}, ':user' => $like->user_id]);

      return $res;
    }
    
    
    
    

  }