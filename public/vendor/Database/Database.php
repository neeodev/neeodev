<?php
	
	class Database
	{
		public static function getConnection($user = 'root', $pass = '', $dbname = 'neeodev', $host = 'localhost'){

			$res = false;


			try {
				$res = new PDO('mysql:host='.$host.';dbname='.$dbname, $user, $pass);
			} catch (PDOException $e) {
				$res = $e->getMessage();
			}

			return $res;
		}
	}
