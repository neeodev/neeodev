<?php
    class Session{

        public function __construct()
        {
            if(empty($_SESSION)){
                session_start();
            }
        }

        public function destroy()
        {
            session_destroy();
        }
        
        public function setAuthenticate($bool = true){
            if(!is_bool($bool)){
                throw new InvalidArgumentException('La valeur n\'est pas un boolean !');
            }
            $_SESSION['auth'] = $bool;
        }

        public function setAttribute($key, $value){
            $_SESSION[$key] = $value;
        }

        public function getAttribute($key){

            $res = false;

            if (isset($_SESSION[$key]) && !empty($_SESSION[$key])) {
                $res = (object) $_SESSION[$key];
            }

            return $res;
        }

        public function setFlash($key, $value){
            $_SESSION['flash'][$key] = $value;
            
        }

        public function getFlash(){
            $flash = $_SESSION['flash'];
            unset($_SESSION['flash']);
            return (object)$flash;
        }

        public function hasFlash(){
            return isset($_SESSION['flash']);
        }

    }