<?php

  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Database/Database.php');

  class Badge{
    private $pdo;

    public function __construct(){
      $this->pdo = Database::getConnection();
    }

    //create
    public function create($badge){
      $sql = 'INSERT INTO badges(name_badge,description_badge,icon_badge) VALUES(:name_badge, :description_badge, :icon_badge)';

      $req = $this->pdo->prepare($sql);
      $res = $req->execute([':name_badge' => $badge->name_badge, ':description_badge' => $badge->description_badge, ':icon_badge' => $badge->icon_badge]);

      return $res;
    }

    public function retrieveAll(){
      $sql = 'SELECT * FROM badges';

      $req = $this->pdo->prepare($sql);
      $req->execute();

      $badges = $req->fetchAll(PDO::FETCH_OBJ);

      return $badges;
    }

    public function retrieveByID($id){
      $sql = 'SELECT * FROM badges WHERE id_badge = :id';

      $req = $this->pdo->prepare($sql);
      $req->execute([':id' => $id]);

      $badge = $req->fetch(PDO::FETCH_OBJ);

      return $badge;
    }

    public function retrieveByName($name){
      $sql = 'SELECT * FROM badges WHERE name_badge = :name_badge';

      $req = $this->pdo->prepare($sql);
      $req->execute([':name_badge' => $name]);

      $badge = $req->fetch(PDO::FETCH_OBJ);

      return $badge;
    }

    public function update($badge){
      $sql = 'UPDATE badges SET name_badge = :name_badge, description_badge = :description_badge, icon_badge = :icon_badge WHERE id_badge = :id';

      $req = $this->pdo->prepare($sql);
      $res = $req->execute([':name_badge' => $badge->name_badge, ':description_badge' => $badge->description_badge, ':icon_badge' => $badge->icon_badge, ':id' => $badge->id_badge]);

      return $res;
    }

    public function delete($badge){
      $sql = 'DELETE FROM badges WHERE id_badge = :id_badge';
      $req = $this->pdo->prepare($sql);
      $res = $req->execute([':id_badge' => $badge->id_badge]);

      if($res){
        $sql = 'DELETE FROM users_badges WHERE badge_id = :id_badge';
        $req = $this->pdo->prepare($sql);
        $res = $req->execute([':id_badge' => $badge->id_badge]);
      }

      return $res;
    }



  }