<?php
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Database/Database.php');
    require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');

    class Security{

        private $pdo;
        private $session;

        public function __construct()
        {
           $this->pdo = Database::getConnection();
           $this->session = new Session();
        }

        public function checkLogin($credential){
            $res = true;
            if (isset($credential) && !empty($credential)){
                $user = $credential->user;
                $pass = $credential->pass;
        
                $sql = 'SELECT * FROM users INNER JOIN grades ON users.grade_id = grades.id_grade WHERE pseudo = :user OR mail = :user';
                $req = $this->pdo->prepare($sql);
                $req->execute(array(':user' => $user));
                $data = $req->fetch(PDO::FETCH_OBJ);
        
                if ($data) {
                    if (password_verify($pass, $data->password)) {
                        $this->session->setAttribute('user', $data);
                        $this->session->setFlash('msg', ['success' => 'Vous êtes connécté ! Bienvenu'.$data->pseudo]);
                        $this->session->setAuthenticate();
                    }else{
                        $this->session->setFlash('user_info', ['user' => $user]);
                        $this->session->setFlash('msg', ['error' => 'Votre nom d\'utilisateur ou votre mot de passe est incorrect !']);
                        $res = false;
                    }
                }else{
                    $this->session->setFlash('user_info', ['user' => $user]);
                    $this->session->setFlash('msg', ['error' => 'Votre nom d\'utilisateur ou votre mot de passe est incorrect !']);
                    $res = false;
                }
        
            }else{
                $this->session->setFlash('msg', ['error' => 'Elle est ou la poulette ! Dites ? Foutez-le camps !']);
                $res = false;
            }
            return $res;
        }

        public function register($data){
            $res = true;

            if ($data->pass == $data->conf) {

                $token = bin2hex(openssl_random_pseudo_bytes(16));

                $sql = 'INSERT INTO users (name, forname, pseudo, mail, password, token, create_date_user) VALUES (:name,:forname,:pseudo,:mail,:pass,:token, :date_user)';
                $req = $this->pdo->prepare($sql);
                
                $rep = $req->execute(array(':name' => $data->name, ':forname' => $data->forname, ':pseudo' => $data->pseudo ,':mail' => $data->mail, ':pass' => password_hash($data->pass, PASSWORD_BCRYPT), ':token' => $token, ':date_user' => date("Y-m-d")));
        
                if ($rep) {
                    $this->session->setFlash('msg', ['success' => 'Votre compte à été créer correctement ! veuillez vérifier vos e-mail pour activer votre compte.']);
                }else{
                    $this->session->setFlash('msg',['error' => 'Une erreur est survenu lors de la création de votre compte ! Vérifier votre mot de passe.']);
                    $this->session->setFlash('user_info', ['name' => $data->name, 'forname' => $data->forname, 'pseudo' => $data->pseudo, 'mail' => $data->mail]);
                    $res = false;
                }
            }else{
                $this->session->setFlash('msg',['error' => 'Une erreur est survenu lors de la création de votre compte ! Vérifier votre mot de passe.']);
                $this->session->setFlash('user_info', ['name' => $data->name, 'forname' => $data->forname, 'pseudo' => $data->pseudo, 'mail' => $data->mail]);
                $res = false;
            }

            return $res;
        }

        public function logout(){
            $this->session = null;
        }
    }