<?php
    class Image{

        private $folder;
        private $token;

        public function __construct($folder){
            $this->folder = $_SERVER['DOCUMENT_ROOT'].'/resource/'.$folder;
            $this->token = bin2hex(openssl_random_pseudo_bytes(16));
        }

        public function upload($img){

            $res = false;

            $fileExt = strtolower(pathinfo($this->folder . basename($img->name), PATHINFO_EXTENSION));
            $file = $this->folder . '/'.$this->token.'.'.$fileExt;

            if($fileExt == "jpg" || $fileExt == "png" || $fileExt == "jpeg") {
                if(move_uploaded_file($img->tmp_name, $file)){
                    $res = $this->token.'.'.$fileExt;
                }
            }

            return $res;
        }

        
        public function update($newImg, $oldImgName){

            $res = $this->delete($oldImgName);
            if ($res) {
                $res = $this->upload($newImg);
            }

            return $res;
        }
        
        public function delete($imgName){
            $res = false;

            $file = $this->folder.'/'.$imgName;

            if(unlink($file)){
                $res = true;
            }

            return $res;
        }

    }