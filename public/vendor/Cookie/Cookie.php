<?php

class Cookie{
    
    public function setCookie($name, $value){
        setcookie($name,$value,time() + (86400 * 30), "/");
    }

    public function retrieve($name){
        $res = false;

        if (isset($_COOKIE[$name])) {
            $res = $_COOKIE[$name];
        }

        return $res;
    }

    public function delete($name){
        $res = false;

        if (isset($_COOKIE[$name])) {
            setcookie($name, '', time() - 3600, "/");
            $res = true;
        }

        return $res;
    }
}