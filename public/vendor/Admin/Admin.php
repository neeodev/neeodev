<?php


require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');

class Admin{

    private $session;

    public function __construct(){
        $this->session = New Session();
    }

    public function isAdmin(){
        $res = false;

        if ($user = $this->session->getAttribute('user')) {
            if ($user->grade_id == 2) {
                $res = true;
            }
        }
        
        return $res;
    }
}