<?php

    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Database/Database.php');

    class Tag{
        private $pdo;
        
        public function __construct(){
            $this->pdo = Database::getConnection();
        }

        public function create($tag){
            $sql = 'INSERT INTO tags(name_tag) VALUES (:name_tag)';
            $req = $this->pdo->prepare($sql);

            $res = $req->execute([':name_tag' => $tag->name_tag]);

            return $res;
        }

        public function retrieveAll(){
            $sql = 'SELECT * FROM tags';
            $req = $this->pdo->prepare($sql);
            $req->execute();

            $tags = $req->fetchAll(PDO::FETCH_OBJ);

            return (object) $tags;
        }

        public function retrieveById($id){
            $sql = 'SELECT * FROM tags WHERE id_tag = :id';
            $req = $this->pdo->prepare($sql);
            $req->execute([':id' => $id]);

            $tag = $req->fetch(PDO::FETCH_OBJ);

            return $tag;
        }

        public function retrieveByName($name){
            $sql = 'SELECT * FROM tags WHERE name_tag = :name_tag';
            $req = $this->pdo->prepare($sql);
            $req->execute([':name_tag' => $name]);

            $tag = $req->fetch(PDO::FETCH_OBJ);

            return $tag;
        }

        public function retrieveByArticle($article){
            $sql = 'SELECT * FROM articles_tags INNER JOIN tags ON articles_tags.tag_id = tags.id_tag WHERE articles_tags.article_id = :id';

            $req = $this->pdo->prepare($sql);
            $req->execute([':id' => $article->id_article]);

            $tags = $req->fetchAll(PDO::FETCH_OBJ);

            return (object) $tags;
        }

        public function delete($tag){
            $sql = 'DELETE FROM tags WHERE id_tag = :id';
            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':id' => $tag->id_tag]);

            return $res;
        }
    }