<?php

    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Database/Database.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Badge/Badge.php');

    class User{

        private $pdo;

        public function __construct(){
            $this->pdo = Database::getConnection();
        }

        public function retrieveAll(){
            $sql = 'SELECT * FROM users INNER JOIN grades ON users.grade_id = grades.id_grade';

            $req = $this->pdo->prepare($sql);
            $req->execute();
            $users = $req->fetchAll(PDO::FETCH_OBJ);

            return (object) $users;
        }

        public function retrieveById($id){

            $sql = 'SELECT * FROM users INNER JOIN grades ON users.grade_id = grades.id_grade WHERE users.id = :id';

            $req = $this->pdo->prepare($sql);
            $req->execute([':id' => $id]);
            $user = $req->fetch(PDO::FETCH_OBJ);

            return (object) $user;
        }

        public function retrieveByUnique($unique){

            $sql = 'SELECT * FROM users INNER JOIN grades ON users.grade_id = grades.id_grade WHERE (users.pseudo = :pseudo) OR (users.mail = :mail)';

            $req = $this->pdo->prepare($sql);
            $req->execute([':pseudo' => $unique, ':mail' => $unique]);
            $user = $req->fetch(PDO::FETCH_OBJ);

            return (object) $user;
        }

        public function retrieveByGrade($grade){
            $users = false;

            $sql = 'SELECT * FROM users INNER JOIN grades ON users.grade_id = grades.id_grade WHERE users.grade_id = :grade';

            $req = $this->pdo->prepare($sql);
            $req->execute([':grade' => $grade]);
            $data = $req->fetchAll(PDO::FETCH_OBJ);

            if (count($data) != 0) {
                $users = $data;
            }

            return (object) $users;
        }
        public function update($newDataUser){

            $sql = 'UPDATE users SET name = :name, forname = :forname, pseudo = :pseudo, mail = :mail, grade_id = :grade_id WHERE id = :id';

            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':name' => $newDataUser->name, ':forname' => $newDataUser->forname, ':pseudo' => $newDataUser->pseudo, ':mail' => $newDataUser->mail, ':grade_id' => $newDataUser->grade_id, ':id' => $newDataUser->id]);
            
            return $res;
        }

        public function activateBadge($user, $badge){
            $sql = 'INSERT INTO users_badges VALUES(:user_id, :badge_id)';

            $req = $this->pdo->prepare($sql);
            $res = $req->execute(['user_id' => $user->id, 'badge_id' => $badge->id_badge]);

            return $res;
        }

        public function setActiveByToken($token){
            $res = false;

            $sql = 'SELECT * FROM users WHERE token = :token';
            $req = $this->pdo->prepare($sql);
            $req->execute([':token' => $token]);

            $data = $req->fetchAll();

            if (count($data) == 1) {
                $sql = 'UPDATE users SET active = 1, token = "" WHERE token = :token';
                $req = $this->pdo->prepare($sql);
                $req->execute([':token' => $token]);
                $res = true;
            }

            return $res;
        }

        public function setActiveByID($id){
            $res = false;

            $sql = 'SELECT * FROM users WHERE id = :id';
            $req = $this->pdo->prepare($sql);
            $req->execute([':id' => $id]);

            $data = $req->fetchAll();

            if (count($data) == 1) {
                $sql = 'UPDATE users SET active = 1, token = "" WHERE id = :id';
                $req = $this->pdo->prepare($sql);
                $req->execute([':id' => $id]);
                $res = true;
            }

            return $res;
        }

        public function setInactiveById($id){
            $res = false;

            $sql = 'SELECT * FROM users WHERE id = :id';
            $req = $this->pdo->prepare($sql);
            $req->execute([':id' => $id]);
            $token = bin2hex(openssl_random_pseudo_bytes(16));


            $data = $req->fetchAll();

            if (count($data) == 1) {
                $sql = 'UPDATE users SET active = 0, token = :token WHERE id = :id';
                $req = $this->pdo->prepare($sql);
                $req->execute([':id' => $id, ':token' => $token]);
                $res = true;
            }

            return $res;
        }

        public function incrementAllReputation($nb){
            $sql = 'UPDATE users SET reputation = reputation + :nb';
            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':nb' => $nb]);

            return $res;
        }

        public function incrementUserReputation($user,$nb){
            $sql = 'UPDATE users SET reputation = reputation + :nb WHERE id = :id';
            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':nb' => $nb, ':id' => $user->id]);

            return $res;
        }

        public function decrementUserReputation($user, $nb){
            $sql = 'UPDATE users SET reputation = reputation - :nb WHERE id = :id';
            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':nb' => $nb, ':id' => $user->id]);

            return $res;
        }

        public function checkBadge($user,$badge){
            $sql = 'SELECT * FROM users_badges WHERE user_id = :user_id AND badge_id = :badge_id';
            $req = $this->pdo->prepare($sql);
            $req->execute([':user_id' => $user->id, ':badge_id' => $badge->id_badge]);

            $badge = $req->fetch(PDO::FETCH_OBJ);

            return $badge;
        }

        public function delete($user){
            $sql = 'DELETE FROM users WHERE id = :id';

            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':id' => $user->id]);

            if($res){
                $sql = 'DELETE FROM users_badges WHERE user_id = :id';

                $req = $this->pdo->prepare($sql);
                $res = $req->execute([':id' => $user->id]);
            }

            return $res;
        }
    }