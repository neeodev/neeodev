<?php

  require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Database/Database.php');

  class Comment{

    private $pdo;

    public function __construct(){

      $this->pdo = Database::getConnection();

    }

    public function create($comment){
      $sql = 'INSERT INTO comments(title_comment, content_comment, user_id, article_id) VALUES(:title, :content, :user_id, :article_id)';

      $req = $this->pdo->prepare($sql);
      $res = $req->execute([':title' => $comment->title_comment, ':content' => $comment->content_comment, ':user_id' => $comment->user_id, 'article_id' => $comment->article_id]);

      return $res;
    }

    public function retrieveByArticle($article){
      $sql = 'SELECT * FROM comments INNER JOIN users ON comments.user_id = users.id WHERE article_id = :article_id';
      $req = $this->pdo->prepare($sql);
      
      $req->execute([':article_id' => $article->id_article]);

      $comments = $req->fetchAll(PDO::FETCH_OBJ);

      return $comments;
    }

    public function retrieveById($id){
      $sql = 'SELECT * FROM comments INNER JOIN users ON comments.user_id = users.id WHERE id_comment = :id';
      $req = $this->pdo->prepare($sql);
      
      $req->execute([':id' => $id]);

      $comment = $req->fetch(PDO::FETCH_OBJ);

      return $comment;
    }

    public function retrieveByUser($user){
      $sql = 'SELECT * FROM comments INNER JOIN users ON comments.user_id = users.id WHERE user_id = :id';
      $req = $this->pdo->prepare($sql);
      
      $req->execute([':id' => $user->id]);

      $comments = $req->fetchAll(PDO::FETCH_OBJ);

      return $comments;
    }

    public function delete($comment){
      $sql = 'DELETE FROM comments where id_comment = :id';
      $req = $this->pdo->prepare($sql);
      
      $res = $req->execute([':id' => $comment->id_comment]);

      return $res;
    }
    
  }