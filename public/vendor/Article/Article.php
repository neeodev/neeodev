<?php

    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Database/Database.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Tag/Tag.php');
    require_once($_SERVER['DOCUMENT_ROOT'].'/vendor/Image/Image.php');

    class Article{

        private $pdo;
        private $modelTag;
        private $modelImage;

        public function __construct(){
            $this->pdo = Database::getConnection();
            $this->modelTag = new Tag();
            $this->modelImage = new Image('article');
        }

        public function create($article){

            $sql = 'INSERT INTO articles(title,content,img,user_id) VALUES (:title,:content,:img,:user_id)';
            $req = $this->pdo->prepare($sql);

            $res = $req->execute([':title' => $article->title, ':content' => $article->content, ':img' => $article->img, ':user_id' => $article->user_id]);
            $tags = $article->tags;

            $article = $this->retrieveByTitle($article->title);

            $article->tags = $tags;

            if($res){
                foreach ($article->tags as $t) {
                    $isInDatabase = $this->modelTag->retrieveByName($t->name_tag);
                    if($isInDatabase == false){
                        $tag = new stdClass();

                        $tag->name_tag = $t->name_tag;

                        $this->modelTag->create($tag);
                        $isInDatabase = $this->modelTag->retrieveByName($t->name_tag);
                    }
                    $t = $isInDatabase;
                    $this->linkArticleTag($article, $t);
                }
            }

            return $res;
        }

        public function retrieveAll(){

            $sql = 'SELECT * FROM articles INNER JOIN users ON articles.user_id = users.id ORDER BY articles.create_date DESC';
            $req = $this->pdo->prepare($sql);
            $req->execute();

            $articles = $req->fetchAll(PDO::FETCH_OBJ);

            foreach ($articles as $a) {
                $a->tags = $this->modelTag->retrieveByArticle($a);
            }
            

            return (object) $articles;
        }

        public function retrieveByAmount($amount){
            $sql = 'SELECT * FROM articles INNER JOIN users ON articles.user_id = users.id ORDER BY articles.create_date DESC LIMIT :amount';
            $req = $this->pdo->prepare($sql);
            $req->bindParam(':amount', $amount, PDO::PARAM_INT);
            $req->execute();

            $articles = $req->fetchAll(PDO::FETCH_OBJ);

            foreach ($articles as $a) {
                $a->tags = $this->modelTag->retrieveByArticle($a);
            }
            

            return (object) $articles;
        }

        public function retrieveByAmountWithOffset($amount, $offset){
            $sql = 'SELECT * FROM articles INNER JOIN users ON articles.user_id = users.id ORDER BY articles.create_date DESC LIMIT :amount OFFSET :offset';
            $req = $this->pdo->prepare($sql);
            $req->bindParam(':offset', $offset, PDO::PARAM_INT);
            $req->bindParam(':amount', $amount, PDO::PARAM_INT);
            $req->execute();

            $articles = $req->fetchAll(PDO::FETCH_OBJ);

            foreach ($articles as $a) {
                $a->tags = $this->modelTag->retrieveByArticle($a);
            }
            

            return (object) $articles;
        }

        public function retrieveById($id){
            $sql = 'SELECT * FROM articles INNER JOIN users ON articles.user_id = users.id WHERE articles.id_article = :id';
            $req = $this->pdo->prepare($sql);
            $req->execute([':id' => $id]);

            $article = $req->fetch(PDO::FETCH_OBJ);

            $article->tags = $this->modelTag->retrieveByArticle($article);

            return $article;
        }

        public function retrieveByTitle($title){
            $sql = 'SELECT * FROM articles INNER JOIN users ON articles.user_id = users.id WHERE articles.title = :title';
            $req = $this->pdo->prepare($sql);
            $req->execute([':title' => $title]);

            $article = $req->fetch(PDO::FETCH_OBJ);

            $article->tags = $this->modelTag->retrieveByArticle($article);

            return $article;
        }

        public function retrieveByUser($user){
            $sql = 'SELECT * FROM articles_tags INNER JOIN tags ON articles_tags.tag_id = tags.id_tag WHERE articles_tags.user_id = :id ORDER BY articles.create_date DESC';
            $req = $this->pdo->prepare($sql);
            $req->execute([':id' => $user->id]);

            $articles = $req->fetchAll(PDO::FETCH_OBJ);

            foreach ($articles as $a) {
                $a->tags = $this->modelTag->retrieveByArticle($a);
            }

            return (object) $articles;
        }

        public function retrieveByTag($tag){
            $sql = 'SELECT * FROM articles_tags INNER JOIN articles ON articles_tags.tag_id = articles.id_article WHERE articles_tags.tag_id = :id ORDER BY articles.create_date DESC';

            $req = $this->pdo->prepare($sql);
            $req->execute([':id' => $tag->id_tag]);

            $articles = $req->fetchAll(PDO::FETCH_OBJ);

            foreach ($articles as $a) {
                $a->tags = $this->modelTag->retrieveByArticle($a);
            }

            return (object) $articles;
        }
        
        public function update($article){
            $sql = 'UPDATE articles SET title = :title, content = :content, img = :img, update_date = :update_date WHERE id_article = :id';
            $update_date = new \DateTime();
            $update_date = $update_date->format('Y-m-d H:i:s');

            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':title' => $article->title, ':content' => $article->content, ':img' => $article->img, ':id' => $article->id_article, ':update_date' => $update_date]);
 
            if($res){
                foreach ($article->tags as $t) {
                    $isInDatabase = $this->modelTag->retrieveByName($t->name_tag);
                    if($isInDatabase == false){
                        $tag = new stdClass();

                        $tag->name_tag = $t->name_tag;

                        $this->modelTag->create($tag);
                        $isInDatabase = $this->modelTag->retrieveByName($t->name_tag);
                    }

                    $t = $isInDatabase;
                    $tags = $this->modelTag->retrieveByArticle($article);

                    foreach ($tags as $ot) {
                        $toDelete = false;
                        foreach ($article->tags as $nt) {
                            if ($nt->name_tag == $ot->name_tag) {
                                $toDelete = true;
                            }
                        }
                        if (!$toDelete) {
                            $sql = 'SELECT  COUNT(*) FROM articles_tags WHERE tag_id = :id';
                            $req = $this->pdo->prepare($sql);
                            $req->execute([':id' => $ot->id_tag]);
            
                            $nbNewLinkWithTag = $req->fetch();
            
                            if ($nbNewLinkWithTag[0] == 1) {
                                $this->modelTag->delete($ot);
                            }

                            $this->unlinkArticleTag($article,$ot);
                        }
                    }
                    $this->linkArticleTag($article, $t);
                }
            }
            
            return $res;
        }

        public function delete($article){
            $tags = $this->modelTag->retrieveByArticle($article);

            foreach ($tags as $t) {
                $sql = 'SELECT  COUNT(*) FROM articles_tags WHERE tag_id = :id';
                $req = $this->pdo->prepare($sql);
                $req->execute([':id' => $t->id_tag]);

                $nbNewLinkWithTag = $req->fetch();

                if ($nbNewLinkWithTag[0] == 1) {
                    $this->modelTag->delete($t);
                }elseif($nbNewLinkWithTag > 1){
                    $this->unlinkArticleTag($article,$t);
                }
            }

            $sql = 'DELETE FROM articles WHERE id_article = :id';
            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':id' => $article->id_article]);

            
            $sql = 'DELETE FROM articles_tags WHERE article_id = :id';
            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':id' => $article->id_article]);
            
            $res = $this->modelImage->delete($article->img);

            return $res;
        }

        private function linkArticleTag($article, $tag){
            $sql = 'INSERT INTO articles_tags VALUES(:article_id, :tag_id)';

            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':article_id' => $article->id_article, ':tag_id' => $tag->id_tag]);

            return $res;
        }

        private function unlinkArticleTag($article, $tag){
            $sql = 'DELETE FROM articles_tags WHERE article_id = :article_id AND tag_id = :tag_id';

            $req = $this->pdo->prepare($sql);
            $res = $req->execute([':article_id' => $article->id_article, ':tag_id' => $tag->id_tag]);

            return $res;
        }

    }