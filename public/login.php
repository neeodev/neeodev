<?php

	require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Session/Session.php');

	if (isset($_POST) && !empty($_POST)) {
		require_once($_SERVER["DOCUMENT_ROOT"].'/vendor/Security/Security.php');
		$security = new Security();
		$auth = $security->checkLogin((object)$_POST);
		if ($auth) {
			header('Location: /');
			exit();
		}else{
			$session = new Session();
		}
	}else{
		$session = new Session();
	}

?>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/head.php') ?>

	<h1 class="page-title">Connexion</h1>

	<form action="" method="post" class="form">
		<?php 
			if($session->hasFlash()){ 
				$flash = (object) $session->getFlash()->user_info; 
			}else{
				$flash = null;
			} 
		?>
		<div class="form-group">
			<input type="text" name="user" placeholder="Pseudo ou mail..." class="input i-50" value="<?php if($flash != null) echo $flash->user ?>">
			<input type="password" name="pass" placeholder="Mot de passe..." class="input i-50">
		</div>
		<button type="submit" class="btn">Se connecter</button>
	</form>

<?php include_once($_SERVER["DOCUMENT_ROOT"].'/templates/footer.php') ?>